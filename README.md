# NAME

ffrstats - Generate reports from aggregate data for Final Fantasy Randomizer


# VERSION

Version 0.1.0


# SYNOPSIS

```shell
# Import a new sample:
bin/ffrstats-import --sample-size 10000 data/RELEASE-FLAGS.csv

# Generate a report for a specific release and flags:
bin/ffrstats-export RELEASE FLAGS > report.html
```

# DESCRIPTION

`ffrstats` is a suite of software designed to store large amounts of data 
extracted from ROMs generated with the [Final Fantasy Randomizer], and 
facilitate the generation of functional, reports to be viewed by 
the average computer user.

The software and system is intended to be used primarily by the 
[Final Fantasy Randomizer] developers, or similarly tech-savvy users.


# DEPENDENCIES

- Ruby 2.4+
- The following gems:
  - `configatron ~> 4.5`
  - `grape ~> 1.1`
  - `sequel ~> 5.14`
  - `pg ~> 1.1`
  - `rake`
- Access to a PostgreSQL database, with the rights to create and remove extensions, tables, schemas, and grant 

As a convenience, a Gemfile is included with the project for users 
of `bundler`, though it is not a requirement.

Modules and programs make no assumptions with regards to the availability of 
`bundler`.

# CONFIGURATION

You will need to supply a configuration file in `etc/conf.d/development.rb`.
(See `etc/conf.d/development.sample.rb`)


# INITIAL SETUP




# TESTING




# AUTHOR

Brian Edmonds `<brian@bedmonds.net>`


# SOURCE

You can contribute to the project through the GitLab repository:

`git clone git@gitlab.com:bedmonds/ffr-stats.git`


# BUGS

Please report any bugs or feature requests through the GitLab issue
tracker:

`https://gitlab.com/bedmonds/ffr-stats/issues`


# COPYRIGHT & LICENSE

Copyright 2018 Brian Edmonds

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


[Final Fantasy Randomizer]: http://finalfantasyrandomizer.com/
