#!/usr/bin/env ruby

# Quick and dirty DB import script for item location data
# defined as the sample csv in ~/data.
require 'optionparser'
require 'pp'

class ImportOptions
  PARSER = 
  def self.parse(args)
    options = {}
    options[:infile] = args.pop unless args.size == 1

    parser = OptionParser.new do |opts|
      opts.banner = 'usage: ffrstats-import [options] [file]'

      opts.separator ''
      opts.separator 'Required:'

      opts.on('-n', '--sample-size SIZE', Integer,
              "Number of seeds tallied for the dump data") do |n|
        options[:sample_size] = n
      end

      opts.separator ''
      opts.separator 'Optional:'

      opts.on_tail('-h', '--help', 'Show this message') do
        puts opts
        exit
      end

      opts.on_tail('--version', 'Show version') do
        puts '0.1.0'
        exit
      end
    end

    parser.parse!(args)
    options
  end
end

options = ImportOptions.parse(ARGV)

lib_path = File.join(__dir__, '..', 'lib')
require_relative "#{lib_path}/ffr_stats"
require_relative "#{lib_path}/ffr_stats/import"

begin
  FFRStats.import(options)
rescue FFRStats::Import::Error => e
  pp e.message
  puts ''
  ImportOptions.parse(%w(-h))
end
