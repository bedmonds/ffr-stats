# VERY quick and dirty report rendering implementation.
# [FIXME][TODO] Clean up. Document. Add new reports.
require 'erb'

require 'ffr_stats/queries'

module FFRStats
  # Top-level module for all output generation from
  # database data.
  module Rendering
    TEMPLATE_ROOT = File.join(__dir__, '..', '..', 'assets', 'tpl').freeze

    def self.template_root
      TEMPLATE_ROOT
    end
  end

  KEY_ITEMS = %w(

  )

  def self.render_key_item_percentage_report(release, flags)
    tpl = File.read(File.join(Rendering.template_root, 'key-item-percentages.html.erb'))
    sample_data = FFRStats::Queries.key_items_only(release, flags)
    ::ERB.new(tpl).result(ItemReport.new(sample_data)._binding)
  end


  class ItemReport
    attr_reader :rows

    def initialize(rows)
      @rows = rows
    end

    def release
      @release ||= rows.first[:release]
    end

    def flags
      @flags ||= rows.first[:flags]
    end

    def sample_size
      @sample_size ||= rows.first[:sample_size]
    end

    def items_by_dungeon
      out = {}

      rows.each do |i|
        out[i[:location]] ||= []
        out[i[:location]] << i
      end

      out
    end

    def _binding
      binding
    end
  end
end
