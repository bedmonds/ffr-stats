module FFRStats
  module Queries
    KEY_ITEMS = %w(Lute Crown Crystal Herb Key TNT Adamant Slab Ruby Rod Floater Chime Tail Cube Bottle Oxyale Canoe
Ship Bridge Canal
    ).freeze

    def key_items_only(release, flags)
      FFRStats.db[:item_location_details]
        .select(:item, :location, :release, :flags, :sample_size)
        .select_append { sum(:percentage).as(:percentage) }
        .select_append {
          sum(:number_of_times_found).as(:number_of_times_found)
        }
        .where(release: release, flags: flags, item: KEY_ITEMS)
        .group_by(:item, :location, :release, :flags, :sample_size)
        .all
    end

    extend self
  end
end
