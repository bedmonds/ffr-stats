require 'csv'

module FFRStats
  # Import data into the running environment's database.
  #
  # = Mandatory arguments
  # [infile]: File to import.
  # [sample_size]: Number of seeds tallied for the data in +infile+.
  #
  # = Optional arguments
  # If these are not specified, the import feature will attempt
  # to deduce them from +infile+'s file name.
  #
  # [release]: Release version of FFR used to generate the seeds tallied 
  #            in +infile+.
  # [flags]:   Flags used to generate the seeds tallied in +infile+.
  #
  def self.import(**options)
    Import::Importer.run(options)
  end

  module Parsing
    class Chest
      # Dungeon where this chest is located.
      attr_reader :dungeon

      # 1-based tileset chest index value for this chest.
      # e.g. The first chest in marsh cave would be 1.
      attr_reader :index

      def initialize(location)
        /(?<dungeon>.*)\s+(?<index>\d+)/ =~ location
        @dungeon, @index = dungeon, index
      end
    end

    class ItemLocation
      attr_reader :item
      attr_reader :chest
      attr_reader :count

      def initialize(chest, item, count)
        @chest = chest
        @item = item
        @count = count
      end

      def to_h
        {
          item: item,
          location: chest.dungeon,
          number_of_times_found: count,
          chest_index: chest.index
        }
      end
    end

    def self.parse(csv_dump)
      out = []

      CSV.foreach(csv_dump, headers: true) do |row|
        hash = row.to_h
        chest = hash.delete('Item Location')

        hash.each do |item, count|
          out.push(ItemLocation.new(Chest.new(chest), item, count))
        end
      end

      out
    end
  end

  module Import
    # Base class for all Import errors.
    class Error < StandardError; end

    class DataSource
      # Release version of FFR used to generate the data in +path+.
      attr_reader :release

      # FFR flag string used to generate the data in +path+.
      attr_reader :flags

      # Format of the data in +path+.
      attr_reader :type
      SUPPORTED_TYPES = %w(csv).freeze

      # Path to the aggregate data for this data source.
      attr_reader :path

      def initialize(path)
        @path = path
        determine_metadata
      end

      def exists?
        File.exists?(path)
      end

      def supported?
        SUPPORTED_TYPES.include?(@type)
      end

      private

      def determine_metadata
        @release, tmp = File.basename(path).split('-')
        @flags, @type = tmp.split('.')

        void_metadata unless @release && @flags && @type
      rescue
        void_metadata
      end

      def void_metadata
        @release = nil
        @flags = nil
        @type = nil
      end
    end

    class Importer
      def self.run(opt_hash)
        new(opt_hash).run
      end

      def initialize(opt_hash)
        raise ArgumentError unless opt_hash.is_a?(Hash)

        @options = opt_hash
        data_source = opt_hash.fetch(:infile) do
          raise Import::Error, 'infile is required'
        end
        @source = DataSource.new(data_source)
      end

      def run
        validate!

        locs = FFRStats::Parsing.parse(source.path)

        # [FIXME] For whatever reason, sequel explodes if we use 
        # "release" and "flags" as-is.
        r, f = release, flags
        setting_id = FFRStats.db.get { get_game_setting_id.function(r, f) }
        
        FFRStats.db.transaction do
          sid = FFRStats.db[:samples].returning(:id).insert({
            settings: setting_id,
            sample_size: sample_size
          }).first.fetch(:id)

          db_locs = locs.map { |i| i.to_h.merge(sample: sid) }

          FFRStats.db[:item_locations].multi_insert(db_locs)
        end
      end

      private

      def validate!
        errors = []

        errors << "File not found: '#{file}'" unless source.exists?

        unless source.supported?
          errors << "Unsupported data source type: '#{source.type}'"
        end

        errors << "Sample size is required" unless sample_size

        raise Import::Error, errors.to_s unless errors.size.zero?
      end

      def source
        @source
      end

      def source?
        File.exists?(source)
      end

      def sample_size
        @options[:sample_size]
      end

      def release
        @options[:release] || @source.release
      end

      def flags
        @options[:flags] || @source.flags
      end
    end
  end
end
