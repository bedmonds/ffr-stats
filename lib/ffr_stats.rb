$:.unshift File.join(__dir__, '..', 'lib')

require 'configatron/core'
require 'sequel'

# Quick and dirty multi-environment support.
module FFRStats
  def self.config
    @config ||= Configatron::RootStore.new
  end

  def self.db
    return @cxn if @cxn

    @cxn = Sequel.connect(FFRStats.config.db.url)
    @cxn.extension(:pg_array, :pg_row)

    @cxn
  end

  def self.env
    @env = ENV['FFR_STATS_ENV'] || 'development'
  end
end

require_relative File.join(*%w(.. etc conf.d).push(FFRStats.env))
