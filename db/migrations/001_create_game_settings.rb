# Adds support for the tracking of game settings by 
# FFR release and flag string.
Sequel.migration do
  up do
    run <<-TABLE_GAME_SETTINGS
      CREATE TABLE game_settings (
        id serial primary key not null,
        label text,
        release text not null,
        flags text not null,

        UNIQUE(release, flags)
      );
    TABLE_GAME_SETTINGS
  end

  down do
    drop_table(:game_settings)
  end
end
