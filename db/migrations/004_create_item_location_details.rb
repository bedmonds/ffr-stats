# Adds a view that adds additional information to
# item locations based on their sample's size.
Sequel.migration do
  up do
    run <<-VIEW_ITEM_LOC_DETAILS
      CREATE VIEW item_location_details AS
        SELECT l.item,
               l.location,
               l.chest_index,
               l.number_of_times_found,
               s.sample_size,
               g.release,
               g.flags,
               (l.number_of_times_found::float / s.sample_size::float) * 100 AS percentage
          FROM item_locations l
          JOIN samples s ON s.id = l.sample
          JOIN game_settings g ON g.id = s.settings;
    VIEW_ITEM_LOC_DETAILS
  end

  down do
    run 'DROP VIEW item_location_details;'
  end
end
