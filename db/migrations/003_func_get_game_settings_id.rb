# Adds a utility function to fetch the unique identifier
# for a given release version and flag string.
#
# If none exists, one is created.
Sequel.migration do
  up do
    run <<-FUNC_MKSETTINGS
      CREATE FUNCTION get_game_setting_id(
        v_release text,
        v_flags text
      ) RETURNS integer SECURITY DEFINER LANGUAGE plpgsql
      AS $$
      DECLARE
        v_id integer;
      BEGIN

        SELECT id
          INTO v_id
          FROM game_settings
         WHERE release = v_release AND flags = v_flags;

        IF NOT FOUND THEN
          INSERT INTO game_settings (release, flags)
               VALUES (v_release, v_flags)
            RETURNING id INTO v_id;
        END IF;

        RETURN v_id;
      END;
      $$;
    FUNC_MKSETTINGS
  end

  down do
    run 'DROP FUNCTION get_game_setting_id(text, text);'
  end
end
