# Adds support for the tracking of item location counts, based on a number 
# of sample seeds for a specific FFR release and flag settings.
Sequel.migration do
  up do
    run <<-TABLE_SAMPLES
      CREATE TABLE samples (
        id serial primary key not null,
        settings integer references game_settings on delete cascade not null,
        label text,
        sample_size integer NOT NULL
      );
    TABLE_SAMPLES

    run <<-TYPE_AREA
      CREATE TYPE location AS ENUM(
        'Coneria', 'Temple of Fiends', 'Elfland',
        'Northwest Castle', 'Marsh Cave', 'Dwarf Cave',
        'Matoya''s Cave', 'Earth Cave', 'Titan''s Tunnel',
        'Gurgu Volcano', 'Ice Cave', 'Castle of Ordeals',
        'Cardia Islands', 'Sea Shrine', 'Waterfall',
        'Mirage Tower', 'Sky Castle', 'Temple of Fiends (Revisited)'
      );
    TYPE_AREA

    run <<-TABLE_ITEM_LOCS
      CREATE TABLE item_locations (
        sample integer references samples on delete cascade not null,
        item text not null,
        location location not null,
        number_of_times_found integer not null default 0,

        -- null for NPCs
        chest_index integer,

        UNIQUE(sample, item, location, chest_index)
      )
    TABLE_ITEM_LOCS
  end

  down do
    drop_table(:item_locations)
    run 'DROP TYPE location'
    drop_table(:samples)
  end
end
